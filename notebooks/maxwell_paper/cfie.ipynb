{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href='http://bempp.com' style='line-height:50px'><img src='../bempp-logo.jpg' style='float:left;height:50px;margin-right:10px'> This tutorial forms part of the documentation for Bempp. Find out more at bempp.com</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Combined field integral equation\n",
    "\n",
    "This tutorial shows how to solve the combined field integral equation (CFIE) for exterior scattering problems, as described in section 7 of <a href='https://bempp.com/documentation/example-scripts-maxwell/'>Scroggs <em>et al</em> (2017)</a>.\n",
    "\n",
    "## Background\n",
    "\n",
    "In this tutorial, we use consider an incident wave $$\\mathbf{E}^\\text{inc}(\\mathbf{x})=\\left(\\begin{array}{c}\\mathrm{e}^{\\mathrm{i}kz}\\\\0\\\\0\\end{array}\\right)$$ scattering off the unit sphere.\n",
    "\n",
    "We let $\\mathbf{E}^\\text{s}$ be the scattered field and look to solve\n",
    "$$\n",
    "\\begin{align}\n",
    "\\textbf{curl}\\,\\textbf{curl}\\,\\mathbf{E}-k^2\\mathbf{E}&=0\\quad\\text{in }\\Omega^\\text{+},\\\\\n",
    "\\mathbf{E}\\times\\nu&=0\\quad\\text{on }\\Gamma,\\\\\n",
    "\\lim_{|\\mathbf{x}|\\to\\infty}\\left(\\textbf{curl}\\,\\mathbf{E}^\\text{s}\\times\\frac{\\mathbf{x}}{|\\mathbf{x}|}-\\mathrm{i}k\\mathbf{E}^\\text{s}\\right)&=0,\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "where $\\mathbf{E}=\\mathbf{E}^\\text{s}+\\mathbf{E}^\\text{inc}$ is the total electric field. This is the same problem as in the <a href='https://bempp.com/2017/07/13/electric-field-integral-equation-efie/'>EFIE</a> and <a href='https://bempp.com/2017/07/18/magnetic-field-integral-equation-mfie/'>MFIE</a> examples.\n",
    "\n",
    "### The CFIE\n",
    "Both the MFIE and the EFIE are efficient for low-frequency problems, but at higher frequencies they are unstable close to interior resonances of the domain. The CFIE is immune to these resonances, so is well suited to high-frequency problems.\n",
    "\n",
    "The CFIE is a linear combination of the MFIE and EFIE. Its strong form is as follows:\n",
    "\n",
    "$$-\\mathsf{R}\\mathsf{E}\\Lambda + (\\tfrac12\\mathsf{Id}+\\mathsf{H})\\Lambda\n",
    "=-\\mathsf{R}(\\tfrac12\\mathsf{Id}+\\mathsf{H})\\gamma_\\mathbf{t}^\\text{+}\\mathbf{E}^\\text{inc}-\\mathsf{E}\\gamma_\\mathbf{t}^\\text{+}\\mathbf{E}^\\text{inc},$$\n",
    "\n",
    "where $\\mathsf{R}$ is a regularisatior operator. In this example, we use $\\mathsf{R}=\\mathsf{E}_{\\mathrm{i}k}$, the electric field boundary operator with wavenumber $\\mathrm{i}k$. More details of the CFIE can be found in <a href='http://ieeexplore.ieee.org/document/1178732/' target='new'>Contopanagos <em>et al</em> (2002)</a>.\n",
    "\n",
    "As we did with the <a href='https://bempp.com/2017/07/13/electric-field-integral-equation-efie/'>EFIE</a> and <a href='https://bempp.com/2017/07/18/magnetic-field-integral-equation-mfie/'>MFIE</a>, we need to carefully choose the spaces used to discretise the CFIE to ensure that all the products of operators are stable."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we do the usual imports, set the wavenumber, and define the incident wave, as in the <a href='https://bempp.com/2017/07/13/electric-field-integral-equation-efie/'>EFIE</a> and <a href='https://bempp.com/2017/07/18/magnetic-field-integral-equation-mfie/'>MFIE</a> examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import bempp.api\n",
    "import numpy as np\n",
    "\n",
    "k = 3\n",
    "\n",
    "grid = bempp.api.shapes.sphere(h=0.1)\n",
    "\n",
    "def incident_field(x):\n",
    "    return np.array([np.exp(1j*k*x[2]), 0.*x[2], 0.*x[2]])\n",
    "\n",
    "def tangential_trace(x, n, domain_index, result):\n",
    "    result[:] = np.cross(incident_field(x), n, axis=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define the multitrace operators with wavenumbers $k$ and $\\mathrm{i}k$, extract the spaces and operators we will need from them, and build a grid function representing the incident wave."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "multitrace = \\\n",
    "    bempp.api.operators.boundary.maxwell.multitrace_operator(\n",
    "        grid, k)\n",
    "multitrace_scaled = \\\n",
    "    bempp.api.operators.boundary.maxwell.multitrace_operator(\n",
    "        grid, 1j*k)\n",
    "identity = bempp.api.operators.boundary.sparse.multitrace_identity(\n",
    "    grid, spaces='maxwell')\n",
    "\n",
    "rwg_space = multitrace.domain_spaces[0]\n",
    "snc_space = multitrace.dual_to_range_spaces[1]\n",
    "bc_space = multitrace.domain_spaces[1]\n",
    "rbc_space = multitrace.dual_to_range_spaces[0]\n",
    "\n",
    "calderon = .5 * identity + multitrace\n",
    "grid_fun = bempp.api.GridFunction(\n",
    "    bc_space, fun=tangential_trace,\n",
    "    dual_space=snc_space)\n",
    "\n",
    "R = multitrace_scaled[0, 1]\n",
    "E1 = multitrace[0, 1]\n",
    "E2 = -multitrace[1, 0]\n",
    "mfie1 = calderon[0, 0]\n",
    "mfie2 = calderon[1, 1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use the operators to form the two sides of the equation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "op = -R * E2 + mfie1\n",
    "rhs = -R * mfie2 * grid_fun - E1 * grid_fun"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we define the incendent field and its tangential trace."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Number of iterations: 12\n"
     ]
    }
   ],
   "source": [
    "co, info, its = bempp.api.linalg.gmres(\n",
    "    op, rhs, use_strong_form=True,\n",
    "    return_iteration_count=True)\n",
    "print(\"Number of iterations:\", its)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By running this example for a range of wavenumbers, we can see that the CFIE is indeed immune to resonances. In the following plot, the CFIE is represented by black diamonds, the <a href='https://bempp.com/2017/07/18/magnetic-field-integral-equation-mfie/'>MFIE</a> by blue squares, and the <a href='https://bempp.com/2017/07/13/electric-field-integral-equation-efie/'>Calder&oacute;n preconditioned EFIE</a> by red circles.\n",
    "<img src='cfie_iterations.png' width=400>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
