{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href='http://bempp.com' style='line-height:50px'><img src='../bempp-logo.jpg' style='float:left;height:50px;margin-right:10px'> This tutorial forms part of the documentation for Bempp. Find out more at bempp.com</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Magnetic field integral equation\n",
    "\n",
    "This tutorial shows how to solve the magnetic field integral equation (MFIE) for exterior scattering problems, as described in section 6 of <a href='https://bempp.com/documentation/example-scripts-maxwell/'>Scroggs <em>et al</em> (2017)</a>.\n",
    "\n",
    "## Background\n",
    "\n",
    "In this tutorial, we use consider an incident wave $$\\mathbf{E}^\\text{inc}(\\mathbf{x})=\\left(\\begin{array}{c}\\mathrm{e}^{\\mathrm{i}kz}\\\\0\\\\0\\end{array}\\right)$$ scattering off the unit sphere.\n",
    "\n",
    "We let $\\mathbf{E}^\\text{s}$ be the scattered field and look to solve\n",
    "$$\n",
    "\\begin{align}\n",
    "\\textbf{curl}\\,\\textbf{curl}\\,\\mathbf{E}-k^2\\mathbf{E}&=0\\quad\\text{in }\\Omega^\\text{+},\\\\\n",
    "\\mathbf{E}\\times\\nu&=0\\quad\\text{on }\\Gamma,\\\\\n",
    "\\lim_{|\\mathbf{x}|\\to\\infty}\\left(\\textbf{curl}\\,\\mathbf{E}^\\text{s}\\times\\frac{\\mathbf{x}}{|\\mathbf{x}|}-\\mathrm{i}k\\mathbf{E}^\\text{s}\\right)&=0,\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "where $\\mathbf{E}=\\mathbf{E}^\\text{s}+\\mathbf{E}^\\text{inc}$ is the total electric field. This is the same problem as solved in the <a href='https://bempp.com/2017/07/13/electric-field-integral-equation-efie/'>EFIE example</a>.\n",
    "\n",
    "### MFIE\n",
    "The (indirect) MFIE uses the representation formula\n",
    "\n",
    "$$\\mathbf{E}^\\text{s}=-\\mathcal{H}\\Lambda,$$\n",
    "\n",
    "and the following boundary integral equation.\n",
    "\n",
    "$$\\left(\\mathsf{H}-\\tfrac12\\mathsf{Id}\\right)\\Lambda=\\gamma_\\mathbf{t}^\\text{+}\\mathbf{E}^\\text{inc}.$$\n",
    "\n",
    "Here, $\\gamma_\\mathbf{t}^\\text{+}$ is the tangential trace of a function, as defined in the <a href='https://bempp.com/2017/07/13/electric-field-integral-equation-efie/'>EFIE example</a>.\n",
    "\n",
    "In <a href='http://ieeexplore.ieee.org/document/5767541/' target='new'>Cools <em>et al</em> (2011)</a>, it was suggested that the robust implementation of the MFIE on non-smooth domains requires the use of the stable space pairings, as described in the <a href='/2017/07/12/the-caldern-projector/'>multitrace example script</a>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we do the usual imports, set the wavenumber, and define the incident wave, as in the <a href='https://bempp.com/2017/07/13/electric-field-integral-equation-efie/'>EFIE example</a>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import bempp.api\n",
    "import numpy as np\n",
    "\n",
    "k = 3\n",
    "\n",
    "grid = bempp.api.shapes.sphere(h=0.1)\n",
    "\n",
    "def incident_field(x):\n",
    "    return np.array([np.exp(1j*k*x[2]), 0.*x[2], 0.*x[2]])\n",
    "\n",
    "def tangential_trace(x, n, domain_index, result):\n",
    "    result[:] = np.cross(incident_field(x), n, axis=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define the multitrace operator, extract the spaces and operator we will need from it, and build a grid function representing the incident wave."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "multitrace = bempp.api.operators.boundary.maxwell.multitrace_operator(\n",
    "    grid, k)\n",
    "identity = bempp.api.operators.boundary.sparse.multitrace_identity(\n",
    "    grid, spaces=\"maxwell\")\n",
    "calderon = .5 * identity - multitrace\n",
    "\n",
    "rwg_space = multitrace.domain_spaces[0]\n",
    "rbc_space = multitrace.dual_to_range_spaces[0]\n",
    "\n",
    "rhs = bempp.api.GridFunction(rwg_space,\n",
    "                             fun=tangential_trace,\n",
    "                             dual_space=rbc_space)\n",
    "op = -calderon[0,0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we solve the discrete system and print the number of iterations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Number of iterations: 20\n"
     ]
    }
   ],
   "source": [
    "sol, info, iterations = bempp.api.linalg.gmres(\n",
    "    op, rhs, return_iteration_count=True)\n",
    "print(\"Number of iterations:\", iterations)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
