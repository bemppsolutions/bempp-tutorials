{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href='http://bempp.com' style='line-height:50px'><img src='bempp-logo.jpg' style='float:left;height:50px;margin-right:10px'> This tutorial forms part of the documentation for Bempp. Find out more at bempp.com</a>\n",
    "\n",
    "This example is based on work by Paulo Fernández and <a href='http://www.mecanica.usm.cl/quienes-somos/10-profesores/42-christopher-cooper'>Christopher Cooper Villagrán</a>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Solvation energy of a protein with a continuum electrostatic model\n",
    "\n",
    "## Background\n",
    "\n",
    "The energy required to dissolve a molecule (that is, moving it from vacuum into a solvent) is known as the solvation energy. This quantity is of interest in several biomolecular applications. In a continuum electrostatic description, the dissolved biomolecule can be modeled with two dielectric regions interfaced by a molecular surface: an outer *solvent* region, usually comprised of water and salt ions, and an inner *protein* region, where the solvent cannot access, which contains point charges at the locations of the atoms.  \n",
    "\n",
    "<img src=\"sketch_protein.png\", style=\"width:300px\">\n",
    "\n",
    "The protein region ($\\Omega^-$) is governed by a Poisson equation with a source term due to the partial charges of the atoms, whereas the solvent region ($\\Omega^\\text{+}$) uses a modified Helmholtz equation to account for the presence of salt. The electrostatic potentials in the two regions, $\\phi^-$ and $\\phi^+$, are given by\n",
    "$$ \\Delta \\phi^- (\\mathbf{x}) = \\sum_k q_k \\delta (\\mathbf{x}_k) \\quad\\text{in }\\Omega^-,$$\n",
    "$$ \\Delta \\phi^+ (\\mathbf{x}) - \\kappa \\phi^+ (\\mathbf{x}) = 0  \\quad\\text{in }\\Omega^+,$$\n",
    "$$ \\phi^- = \\phi^+\\quad\\text{on }\\Gamma$$\n",
    "$$\\epsilon^- \\frac{\\partial \\phi^-}{\\partial \\nu} = \\epsilon^+ \\frac{\\partial \\phi^+}{\\partial \\nu}\\quad\\text{on }\\Gamma$$\n",
    "where $q_k$ and $\\mathbf{x}_k$ are the charge and position of each molecule atom, $\\kappa$ is the inverse of the <a href='https://en.wikipedia.org/wiki/Debye_length' target='new'>Debye length</a>.\n",
    "\n",
    "\n",
    "\n",
    "Following the work by <a href='http://onlinelibrary.wiley.com/doi/10.1002/jcc.540110911/abstract' target='new'>Yoon and Lenhoff</a>, the system of PDEs with interface conditions can be formulated in terms of boundary operators, as follows:\n",
    "\n",
    "$$\n",
    "\\begin{bmatrix}\n",
    "    \\tfrac12\\mathsf{Id} + \\mathsf{K}_L &  -\\mathsf{V}_L \\\\\n",
    "    \\tfrac12\\mathsf{Id} - \\mathsf{K}_H &  \\frac{\\epsilon_1}{\\epsilon_2}\\mathsf{V}_H\n",
    "\\end{bmatrix}\n",
    "\\left\\{\n",
    "\\begin{matrix}\n",
    "    \\phi^- \\\\\n",
    "    \\frac{\\partial \\phi^-}{\\partial \\mathbf{\\nu}}\n",
    "\\end{matrix}\n",
    "\\right\\}\n",
    "= \\left\\{\n",
    "\\begin{matrix}\n",
    "    \\sum_k q_k \\\\\n",
    "    0\n",
    "\\end{matrix}\n",
    "\\right\\}\n",
    "$$\n",
    "\n",
    "where $\\mathsf{V}_L$ and $\\mathsf{K}_L$ are the <a href='https://bempp.com/2017/07/11/available_operators/'>single and double layer boundary operators</a> for the <a href='https://bempp.com/tag/laplace'>Laplace</a> kernel (Poisson equation) and $\\mathsf{V}_H$ and $\\mathsf{K}_H$ are the corresponding operators for the <a href='https://bempp.com/tag/modified-helmholtz'>modified Helmholtz</a> kernel (Poisson-Boltzmann or Yukawa equation).\n",
    "\n",
    "From there, we can compute the *reaction potential* ($\\phi_\\text{reac} = \\phi^--\\phi_\\text{Coulomb}$) at the locations of the atoms using\n",
    "\n",
    "$$\\phi_\\text{reac} = \\mathcal{K}_L\\left[ \\frac{\\partial \\phi}{\\partial n} \\right] - \\mathcal{V}_L \\left[ \\phi \\right], $$\n",
    "where $\\mathcal{V}_L$ and $\\mathcal{K}_L$ are the single and double layer potential operators for the Laplace kernel.\n",
    "We then obtain the solvation energy using\n",
    "\n",
    "$$ \\Delta G_\\text{solv} = \\tfrac12 \\sum_{k=1}^{N_q} q_k \\phi_\\text{reac}(\\mathbf{x}_k) $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example\n",
    "\n",
    "### Molecular structure and mesh\n",
    "\n",
    "As an example, we will calculate the solvation energy of the bovine pancreatic trypsin inhibitor. The molecular crystal structure is available the protein data bank ([PDB code 5PTI](https://www.rcsb.org/pdb/explore.do?structureId=5PTI)), and we obtained the atomic radii and charges with [pdb2pqr](http://www.poissonboltzmann.org/), resulting in a `.pqr` file.\n",
    "\n",
    "There are several molecular surface definitions which can be used. In this case, we use the *solvent-excluded surface* (SES), which is the result of rolling a spherical probe of the size of a water molecule around the protein, and tracking the contact points. We mesh the SES with [`msms`](http://mgl.scripps.edu/people/sanner/html/msms_home.html), and reformat the result to a `.msh` file. \n",
    "\n",
    "First, let's import the required libraries and mesh and read the `.pqr` file for the charges."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "fa6332bd9a6b4812b9a69cc8581c6b45",
       "version_major": 2,
       "version_minor": 0
      },
      "text/html": [
       "<p>Failed to display Jupyter Widget of type <code>Renderer</code>.</p>\n",
       "<p>\n",
       "  If you're reading this message in the Jupyter Notebook or JupyterLab Notebook, it may mean\n",
       "  that the widgets JavaScript is still loading. If this message persists, it\n",
       "  likely means that the widgets JavaScript library is either not installed or\n",
       "  not enabled. See the <a href=\"https://ipywidgets.readthedocs.io/en/stable/user_install.html\">Jupyter\n",
       "  Widgets Documentation</a> for setup instructions.\n",
       "</p>\n",
       "<p>\n",
       "  If you're reading this message in another frontend (for example, a static\n",
       "  rendering on GitHub or <a href=\"https://nbviewer.jupyter.org/\">NBViewer</a>),\n",
       "  it may mean that your frontend doesn't currently support widgets.\n",
       "</p>\n"
      ],
      "text/plain": [
       "Renderer(background='white', background_opacity=1.0, camera=PerspectiveCamera(fov=20.0, position=[98.12899999999999, 73.984, 87.23299999999999], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0]), controls=[OrbitControls(controlling=PerspectiveCamera(fov=20.0, position=[98.12899999999999, 73.984, 87.23299999999999], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0]), target=[0.0, 0.0, 0.0])], effect=None, scene=Scene(children=[Line(geometry=PlainGeometry(colors=['red', 'red', 'green', 'green', 'blue', 'blue'], vertices=array([[ 14.89700031,  -5.30900002, -15.88899994],\n",
       "       [ 42.64099884,  -5.30900002, -15.88899994],\n",
       "       [ 14.89700031,  -5.30900002, -15.88899994],\n",
       "       [ 14.89700031,  21.12199974, -15.88899994],\n",
       "       [ 14.89700031,  -5.30900002, -15.88899994],\n",
       "       [ 14.89700031,  -5.30900002,  18.48500061]], dtype=float32)), material=LineBasicMaterial(linewidth=10.0, vertexColors='VertexColors'), position=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0], type='LinePieces', up=[0.0, 1.0, 0.0]), Mesh(geometry=PlainGeometry(faceColors=array([[[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]],\n",
       "\n",
       "       [[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]],\n",
       "\n",
       "       [[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]],\n",
       "\n",
       "       ..., \n",
       "       [[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]],\n",
       "\n",
       "       [[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]],\n",
       "\n",
       "       [[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]]], dtype=float32), faces=array([[   0,    1,    2],\n",
       "       [   2,    1,    3],\n",
       "       [   1,    4,    3],\n",
       "       ..., \n",
       "       [3495, 3130, 3132],\n",
       "       [3495, 3132, 3499],\n",
       "       [3499, 3132, 3134]], dtype=uint32), vertices=array([[ 16.75900078,   9.05200005,  -9.56999969],\n",
       "       [ 15.68700027,   9.53999996,  -9.02299976],\n",
       "       [ 17.02700043,   9.94099998,  -9.83699989],\n",
       "       ..., \n",
       "       [ 19.96699905,  -2.65300012, -11.14000034],\n",
       "       [ 19.96699905,  -2.15300012, -12.00599957],\n",
       "       [ 19.6989994 ,  -1.653     , -11.14000034]], dtype=float32)), material=LambertMaterial(envMap=None, lightMap=None, map=None, specularMap=None, vertexColors='VertexColors'), position=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0]), Mesh(geometry=PlainGeometry(faceColors=array([[[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]],\n",
       "\n",
       "       [[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]],\n",
       "\n",
       "       [[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]],\n",
       "\n",
       "       ..., \n",
       "       [[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]],\n",
       "\n",
       "       [[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]],\n",
       "\n",
       "       [[ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.],\n",
       "        [ 1.,  1.,  1.]]], dtype=float32), faces=array([[   0,    1,    2],\n",
       "       [   2,    1,    3],\n",
       "       [   1,    4,    3],\n",
       "       ..., \n",
       "       [3495, 3130, 3132],\n",
       "       [3495, 3132, 3499],\n",
       "       [3499, 3132, 3134]], dtype=uint32), vertices=array([[ 16.75900078,   9.05200005,  -9.56999969],\n",
       "       [ 15.68700027,   9.53999996,  -9.02299976],\n",
       "       [ 17.02700043,   9.94099998,  -9.83699989],\n",
       "       ..., \n",
       "       [ 19.96699905,  -2.65300012, -11.14000034],\n",
       "       [ 19.96699905,  -2.15300012, -12.00599957],\n",
       "       [ 19.6989994 ,  -1.653     , -11.14000034]], dtype=float32)), material=PhongMaterial(color='black', envMap=None, lightMap=None, map=None, specularMap=None, wireframe=True), position=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0]), AmbientLight(position=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0])], position=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0]))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "39858a16bb4746a89e99dd55ad4640ab",
       "version_major": 2,
       "version_minor": 0
      },
      "text/html": [
       "<p>Failed to display Jupyter Widget of type <code>Label</code>.</p>\n",
       "<p>\n",
       "  If you're reading this message in the Jupyter Notebook or JupyterLab Notebook, it may mean\n",
       "  that the widgets JavaScript is still loading. If this message persists, it\n",
       "  likely means that the widgets JavaScript library is either not installed or\n",
       "  not enabled. See the <a href=\"https://ipywidgets.readthedocs.io/en/stable/user_install.html\">Jupyter\n",
       "  Widgets Documentation</a> for setup instructions.\n",
       "</p>\n",
       "<p>\n",
       "  If you're reading this message in another frontend (for example, a static\n",
       "  rendering on GitHub or <a href=\"https://nbviewer.jupyter.org/\">NBViewer</a>),\n",
       "  it may mean that your frontend doesn't currently support widgets.\n",
       "</p>\n"
      ],
      "text/plain": [
       "Label(value='x: red; y: green; z: blue')"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "53c9a86e91b94648bfda2e0d64f5afca",
       "version_major": 2,
       "version_minor": 0
      },
      "text/html": [
       "<p>Failed to display Jupyter Widget of type <code>Checkbox</code>.</p>\n",
       "<p>\n",
       "  If you're reading this message in the Jupyter Notebook or JupyterLab Notebook, it may mean\n",
       "  that the widgets JavaScript is still loading. If this message persists, it\n",
       "  likely means that the widgets JavaScript library is either not installed or\n",
       "  not enabled. See the <a href=\"https://ipywidgets.readthedocs.io/en/stable/user_install.html\">Jupyter\n",
       "  Widgets Documentation</a> for setup instructions.\n",
       "</p>\n",
       "<p>\n",
       "  If you're reading this message in another frontend (for example, a static\n",
       "  rendering on GitHub or <a href=\"https://nbviewer.jupyter.org/\">NBViewer</a>),\n",
       "  it may mean that your frontend doesn't currently support widgets.\n",
       "</p>\n"
      ],
      "text/plain": [
       "Checkbox(value=True, description='Show coordinate axes')"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import bempp.api\n",
    "bempp.api.set_ipython_notebook_viewer()\n",
    "\n",
    "grid = bempp.api.import_grid('5pti_d1.msh')\n",
    "grid.plot()\n",
    "\n",
    "import numpy as np\n",
    "q, x_q = np.array([]), np.empty((0,3))\n",
    "\n",
    "ep_in = 4.\n",
    "ep_ex = 80.\n",
    "k = 0.125\n",
    "\n",
    "# Read charges and coordinates from the .pqr file\n",
    "molecule_file = open('5pti.pqr', 'r').read().split('\\n')\n",
    "for line in molecule_file:\n",
    "    line = line.split()\n",
    "    if len(line)==0 or line[0]!='ATOM': continue\n",
    "    q = np.append( q, float(line[8]))\n",
    "    x_q = np.vstack(( x_q, np.array(line[5:8]).astype(float) ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we compute the potential due to the charges on the boundary, required for the right-hand side"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def charges_fun(x, n, domain_index, result):\n",
    "    global q, x_q, ep_in\n",
    "    result[:] = np.sum(q/np.linalg.norm( x - x_q, axis=1 ))/(4*np.pi*ep_in)\n",
    "\n",
    "def zero(x, n, domain_index, result):\n",
    "    result[:] = 0\n",
    "    \n",
    "dirichl_space = bempp.api.function_space(grid, \"DP\", 0)\n",
    "neumann_space = bempp.api.function_space(grid, \"DP\", 0)\n",
    "\n",
    "charged_grid_fun = bempp.api.GridFunction(dirichl_space, fun=charges_fun)\n",
    "zero_grid_fun = bempp.api.GridFunction(neumann_space, fun=zero)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we generate the $2\\times 2$ block matrix with the single and double layer operators of the Laplace and modified Helmholtz kernels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Define Operators\n",
    "from bempp.api.operators.boundary import sparse, laplace, modified_helmholtz\n",
    "identity = sparse.identity(dirichl_space, dirichl_space, dirichl_space)\n",
    "slp_in   = laplace.single_layer(neumann_space, dirichl_space, dirichl_space)\n",
    "dlp_in   = laplace.double_layer(dirichl_space, dirichl_space, dirichl_space)\n",
    "slp_out  = modified_helmholtz.single_layer(neumann_space, dirichl_space, dirichl_space, k)\n",
    "dlp_out  = modified_helmholtz.double_layer(dirichl_space, dirichl_space, dirichl_space, k)\n",
    "\n",
    "# Matrix Assembly\n",
    "blocked = bempp.api.BlockedOperator(2, 2)\n",
    "blocked[0, 0] = 0.5*identity + dlp_in\n",
    "blocked[0, 1] = -slp_in\n",
    "blocked[1, 0] = 0.5*identity - dlp_out\n",
    "blocked[1, 1] = (ep_in/ep_ex)*slp_out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now use `gmres` to solve the system. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The linear system was solved in 273 iterations\n"
     ]
    }
   ],
   "source": [
    "sol, info, it_count = bempp.api.linalg.gmres(blocked, [charged_grid_fun, zero_grid_fun],\n",
    "                                            use_strong_form=True, return_iteration_count=True, tol=1e-3)\n",
    "\n",
    "print(\"The linear system was solved in {0} iterations\".format(it_count))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The two following `GridFunction` calls store the calculated boundary potential data (separated by $\\phi$ and $\\frac{\\partial \\phi}{\\partial n}$) for visualization purposes. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/usr/lib/python3/dist-packages/numpy/core/numeric.py:747: ComplexWarning: Casting complex values to real discards the imaginary part\n",
      "  arr = array(a, dtype=dtype, order=order, copy=False, subok=subok)\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "a43bbe2634ba4cecb52cca0b9f0110d6",
       "version_major": 2,
       "version_minor": 0
      },
      "text/html": [
       "<p>Failed to display Jupyter Widget of type <code>Renderer</code>.</p>\n",
       "<p>\n",
       "  If you're reading this message in the Jupyter Notebook or JupyterLab Notebook, it may mean\n",
       "  that the widgets JavaScript is still loading. If this message persists, it\n",
       "  likely means that the widgets JavaScript library is either not installed or\n",
       "  not enabled. See the <a href=\"https://ipywidgets.readthedocs.io/en/stable/user_install.html\">Jupyter\n",
       "  Widgets Documentation</a> for setup instructions.\n",
       "</p>\n",
       "<p>\n",
       "  If you're reading this message in another frontend (for example, a static\n",
       "  rendering on GitHub or <a href=\"https://nbviewer.jupyter.org/\">NBViewer</a>),\n",
       "  it may mean that your frontend doesn't currently support widgets.\n",
       "</p>\n"
      ],
      "text/plain": [
       "Renderer(background='white', background_opacity=1.0, camera=PerspectiveCamera(fov=20.0, position=[98.12899999999999, 73.984, 87.23299999999999], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0]), controls=[OrbitControls(controlling=PerspectiveCamera(fov=20.0, position=[98.12899999999999, 73.984, 87.23299999999999], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0]), target=[0.0, 0.0, 0.0])], effect=None, scene=Scene(children=[Line(geometry=PlainGeometry(colors=['red', 'red', 'green', 'green', 'blue', 'blue'], vertices=array([[ 14.89700031,  -5.30900002, -15.88899994],\n",
       "       [ 42.64099884,  -5.30900002, -15.88899994],\n",
       "       [ 14.89700031,  -5.30900002, -15.88899994],\n",
       "       [ 14.89700031,  21.12199974, -15.88899994],\n",
       "       [ 14.89700031,  -5.30900002, -15.88899994],\n",
       "       [ 14.89700031,  -5.30900002,  18.48500061]], dtype=float32)), material=LineBasicMaterial(linewidth=10.0, vertexColors='VertexColors'), position=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0], type='LinePieces', up=[0.0, 1.0, 0.0]), Mesh(geometry=PlainGeometry(faceColors=array([[[ 0.22454143,  1.        ,  0.74320048],\n",
       "        [ 0.22454143,  1.        ,  0.74320048],\n",
       "        [ 0.22454143,  1.        ,  0.74320048]],\n",
       "\n",
       "       [[ 0.23719165,  1.        ,  0.73055029],\n",
       "        [ 0.23719165,  1.        ,  0.73055029],\n",
       "        [ 0.23719165,  1.        ,  0.73055029]],\n",
       "\n",
       "       [[ 0.31309298,  1.        ,  0.65464896],\n",
       "        [ 0.31309298,  1.        ,  0.65464896],\n",
       "        [ 0.31309298,  1.        ,  0.65464896]],\n",
       "\n",
       "       ..., \n",
       "       [[ 0.99620491,  0.93028325,  0.        ],\n",
       "        [ 0.99620491,  0.93028325,  0.        ],\n",
       "        [ 0.99620491,  0.93028325,  0.        ]],\n",
       "\n",
       "       [[ 0.98355472,  0.94480753,  0.        ],\n",
       "        [ 0.98355472,  0.94480753,  0.        ],\n",
       "        [ 0.98355472,  0.94480753,  0.        ]],\n",
       "\n",
       "       [[ 0.89500314,  1.        ,  0.07273877],\n",
       "        [ 0.89500314,  1.        ,  0.07273877],\n",
       "        [ 0.89500314,  1.        ,  0.07273877]]], dtype=float32), faces=array([[   0,    1,    2],\n",
       "       [   2,    1,    3],\n",
       "       [   1,    4,    3],\n",
       "       ..., \n",
       "       [3495, 3130, 3132],\n",
       "       [3495, 3132, 3499],\n",
       "       [3499, 3132, 3134]], dtype=uint32), vertices=array([[ 16.75900078,   9.05200005,  -9.56999969],\n",
       "       [ 15.68700027,   9.53999996,  -9.02299976],\n",
       "       [ 17.02700043,   9.94099998,  -9.83699989],\n",
       "       ..., \n",
       "       [ 19.96699905,  -2.65300012, -11.14000034],\n",
       "       [ 19.96699905,  -2.15300012, -12.00599957],\n",
       "       [ 19.6989994 ,  -1.653     , -11.14000034]], dtype=float32)), material=LambertMaterial(envMap=None, lightMap=None, map=None, specularMap=None, vertexColors='VertexColors'), position=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0]), Mesh(geometry=PlainGeometry(faceColors=array([[[ 0.22454143,  1.        ,  0.74320048],\n",
       "        [ 0.22454143,  1.        ,  0.74320048],\n",
       "        [ 0.22454143,  1.        ,  0.74320048]],\n",
       "\n",
       "       [[ 0.23719165,  1.        ,  0.73055029],\n",
       "        [ 0.23719165,  1.        ,  0.73055029],\n",
       "        [ 0.23719165,  1.        ,  0.73055029]],\n",
       "\n",
       "       [[ 0.31309298,  1.        ,  0.65464896],\n",
       "        [ 0.31309298,  1.        ,  0.65464896],\n",
       "        [ 0.31309298,  1.        ,  0.65464896]],\n",
       "\n",
       "       ..., \n",
       "       [[ 0.99620491,  0.93028325,  0.        ],\n",
       "        [ 0.99620491,  0.93028325,  0.        ],\n",
       "        [ 0.99620491,  0.93028325,  0.        ]],\n",
       "\n",
       "       [[ 0.98355472,  0.94480753,  0.        ],\n",
       "        [ 0.98355472,  0.94480753,  0.        ],\n",
       "        [ 0.98355472,  0.94480753,  0.        ]],\n",
       "\n",
       "       [[ 0.89500314,  1.        ,  0.07273877],\n",
       "        [ 0.89500314,  1.        ,  0.07273877],\n",
       "        [ 0.89500314,  1.        ,  0.07273877]]], dtype=float32), faces=array([[   0,    1,    2],\n",
       "       [   2,    1,    3],\n",
       "       [   1,    4,    3],\n",
       "       ..., \n",
       "       [3495, 3130, 3132],\n",
       "       [3495, 3132, 3499],\n",
       "       [3499, 3132, 3134]], dtype=uint32), vertices=array([[ 16.75900078,   9.05200005,  -9.56999969],\n",
       "       [ 15.68700027,   9.53999996,  -9.02299976],\n",
       "       [ 17.02700043,   9.94099998,  -9.83699989],\n",
       "       ..., \n",
       "       [ 19.96699905,  -2.65300012, -11.14000034],\n",
       "       [ 19.96699905,  -2.15300012, -12.00599957],\n",
       "       [ 19.6989994 ,  -1.653     , -11.14000034]], dtype=float32)), material=PhongMaterial(color='black', envMap=None, lightMap=None, map=None, specularMap=None, wireframe=True), position=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0]), AmbientLight(position=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0])], position=[0.0, 0.0, 0.0], scale=[1.0, 1.0, 1.0], up=[0.0, 1.0, 0.0]))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "22f30fdedc924860bf2c73d8cc10539d",
       "version_major": 2,
       "version_minor": 0
      },
      "text/html": [
       "<p>Failed to display Jupyter Widget of type <code>VBox</code>.</p>\n",
       "<p>\n",
       "  If you're reading this message in the Jupyter Notebook or JupyterLab Notebook, it may mean\n",
       "  that the widgets JavaScript is still loading. If this message persists, it\n",
       "  likely means that the widgets JavaScript library is either not installed or\n",
       "  not enabled. See the <a href=\"https://ipywidgets.readthedocs.io/en/stable/user_install.html\">Jupyter\n",
       "  Widgets Documentation</a> for setup instructions.\n",
       "</p>\n",
       "<p>\n",
       "  If you're reading this message in another frontend (for example, a static\n",
       "  rendering on GitHub or <a href=\"https://nbviewer.jupyter.org/\">NBViewer</a>),\n",
       "  it may mean that your frontend doesn't currently support widgets.\n",
       "</p>\n"
      ],
      "text/plain": [
       "VBox(children=(Label(value='x: red; y: green; z: blue'), VBox(children=(Label(value='Lower bound: -0.0012394195996019166'), Label(value='Upper bound: 0.0013797137361278556'))), HBox(children=(FloatText(value=-0.0012394195996019166, description='vmin:'), FloatText(value=0.0013797137361278556, description='vmax:'))), HBox(children=(Checkbox(value=True, description='Enable wireframe'), Checkbox(value=True, description='Show coordinate axes')))))"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "solution_dirichl, solution_neumann = sol\n",
    "solution_dirichl.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To compute $\\phi_\\text{reac}$ at the atoms' locations, we use `operators.potential` to then multiply by the charge and add to compute $\\Delta G_\\text{solv}$. The `332.064` term is just a unit conversion constant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Total solvation energy: -352.82 [kcal/Mol]\n"
     ]
    }
   ],
   "source": [
    "slp_q = bempp.api.operators.potential.laplace.single_layer(neumann_space, x_q.transpose())\n",
    "dlp_q = bempp.api.operators.potential.laplace.double_layer(dirichl_space, x_q.transpose())\n",
    "phi_q = slp_q * solution_neumann - dlp_q * solution_dirichl\n",
    "\n",
    "# total dissolution energy applying constant to get units [kcal/mol]\n",
    "total_energy = 2 * np.pi * 332.064 * np.sum(q * phi_q).real\n",
    "print(\"Total solvation energy: {:7.2f} [kcal/Mol]\".format(total_energy))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
