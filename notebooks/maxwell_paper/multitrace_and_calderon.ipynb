{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href='http://bempp.com' style='line-height:50px'><img src='../bempp-logo.jpg' style='float:left;height:50px;margin-right:10px'> This tutorial forms part of the documentation for Bempp. Find out more at bempp.com</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The multitrace operator and Calderón projector\n",
    "\n",
    "This tutorial shows how to create and use a Calderón projectors and multitrace operators for Maxwell problems, as described in section 4 of <a href='https://bempp.com/documentation/example-scripts-maxwell/'>Scroggs <em>et al</em> (2017)</a>.\n",
    "\n",
    "## Background\n",
    "For Maxwell problems, we define the multitrace operator $\\mathsf{A}$ by\n",
    "\n",
    "$$\\mathsf{A}=\\begin{bmatrix}\\mathsf{H}&\\mathsf{E}\\\\-\\mathsf{E}&\\mathsf{H}\\end{bmatrix},$$\n",
    "\n",
    "where $\\mathsf{H}$ and $\\mathsf{E}$ are the <a href='https://bempp.com/2017/07/11/available_operators/'>magnetic and electric field boundary operators</a>.\n",
    "\n",
    "The interior Calder&oacute;n projector $\\mathcal{C}^\\text{--}$ is defined by\n",
    "\n",
    "$$\\mathcal{C}^\\text{--}=\\tfrac12\\mathsf{Id}+\\mathsf{A},$$\n",
    "\n",
    "and the exterior Calder&oacute;n projector $\\mathcal{C}^\\text{+}$ is defined by\n",
    "\n",
    "$$\\mathcal{C}^\\text{+}=\\tfrac12\\mathsf{Id}-\\mathsf{A}.$$\n",
    "\n",
    "If $\\mathbf{E}$ and $\\mathbf{H}$ are valid Cauchy data for a exterior Maxwell problem, then\n",
    "\n",
    "$$\\mathcal{C}^\\text{+}\\begin{bmatrix}\\mathbf{E}\\\\\\mathbf{H}\\end{bmatrix}=\\begin{bmatrix}\\mathbf{E}\\\\\\mathbf{H}\\end{bmatrix}.$$\n",
    "\n",
    "It follows from the property $\\left[\\mathcal{C}^\\text{+}\\right]^2=\\mathcal{C}^\\text{+}$ that for any tangential functions $\\mathbf{A}$ and $\\mathbf{B}$, the functions $\\mathbf{E}$ and $\\mathbf{H}$ defined by\n",
    "\n",
    "$$\\begin{bmatrix}\\mathbf{E}\\\\\\mathbf{H}\\end{bmatrix}=\\mathcal{C}^\\text{+}\\begin{bmatrix}\\mathbf{A}\\\\\\mathbf{B}\\end{bmatrix}$$\n",
    "\n",
    "are valid Cauchy data.\n",
    "\n",
    "### Discretisation\n",
    "Let $\\mathsf{B}$ and $\\mathsf{C}$ be two boundary operators. If the product $\\mathsf{BC}$ were discretised, the result would $B_hM^{-1}C_h$, where $M$ is the identity matrix between the ``range`` and ``dual_to_range`` spaces of $\\mathsf{C}$.\n",
    "\n",
    "If standard Raviart&ndash;Thomas (RT) and N&eacute;d&eacute;lec (NC) basic functions are used to discretise $\\mathsf{C}$, the matrix $M$ will be numerically singular. To avoid this problem, a stable pairing of RT and Buffa&ndash;Christiansen (BC) functions should be used.\n",
    "\n",
    "In Bempp, the function ``bempp.api.operators.boundary.maxwell.multitrace_operator`` will automatically use the correct spaces so that all terms in the product $\\mathsf{A}^2$ are stable. This is discussed in more detail in section 4 of <a href='https://bempp.com/documentation/example-scripts-maxwell/'>Scroggs <em>et al</em> (2017)</a>.\n",
    "\n",
    "## Implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we import everything we need and set the wavenumber."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import bempp.api\n",
    "from bempp.api.operators.boundary import maxwell\n",
    "from bempp.api.operators.boundary import sparse\n",
    "import numpy as np\n",
    "\n",
    "k = 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We make the meshed cube on which we will define our operators."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "grid = bempp.api.shapes.cube(h=0.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we make the multitrace operator and identity on the grid. Here, we must rell the multitrace identity to use vector ``\"maxwell\"`` spaces instead of the default scalar spaces used for Laplace and Helmholtz."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "multitrace = maxwell.multitrace_operator(grid, k)\n",
    "identity = sparse.multitrace_identity(grid, spaces='maxwell')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define the exterior Calderón projector, $\\mathcal{C}^\\text{+} = \\tfrac12\\mathsf{Id}-\\mathsf{A}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "calderon = 0.5 * identity - multitrace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define grid functions using a Python function. These functions will not be valid Cauchy data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def tangential_trace(x, n, domain_index, result):\n",
    "    result[:] = np.cross(np.array([1,0,0]),n)\n",
    "\n",
    "electric_trace = bempp.api.GridFunction(\n",
    "    space=calderon.domain_spaces[0],\n",
    "    fun=tangential_trace,\n",
    "    dual_space=calderon.dual_to_range_spaces[0])\n",
    "\n",
    "magnetic_trace = bempp.api.GridFunction(\n",
    "    space=calderon.domain_spaces[1],\n",
    "    fun=tangential_trace,\n",
    "    dual_space=calderon.dual_to_range_spaces[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now apply the Calderón projector to the functions twice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "traces_1 = calderon * [electric_trace, magnetic_trace]\n",
    "traces_2 = calderon * traces_1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As explained above, the functions in ``traces_1`` should be valid Cauchy data. Additionally, ``traces_2`` should be equal to ``traces_1`` because $\\left[\\mathcal{C}^\\text{+}\\right]^2=\\mathcal{C}^\\text{+}$. To verify this, we plot the relative difference between the functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "electric_error = (traces_2[0] - traces_1[0]).l2_norm() / traces_1[0].l2_norm()\n",
    "magnetic_error = (traces_2[1] - traces_1[1]).l2_norm() / traces_1[1].l2_norm()\n",
    "print(\"Electric error is \", electric_error)\n",
    "print(\"Magnetic error is \", magnetic_error)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As expected, the errors are small."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "traces_1[0].plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
